import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  addTodo(text:string,status:boolean){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/uid/'+user.uid+'/todos').push({'text':text, 'status':false});
    })
  }

  deleteTodo(key:string)
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/uid/'+user.uid+'/todos').remove(key);
    })
  }

  updateTodo(key:string, text:string, status:boolean){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/uid/'+user.uid+'/todos').update(key,{'text':text,'status':status});
    })
  }


  updateStatus(key:string, text:string, status:boolean)
  {
    this.authService.user.subscribe(user =>{
      this.db.list('/users/uid/'+user.uid+'/todos').update(key,{'text':text, 'status':status});
    })
    
  }

  constructor(private authService: AuthService,
              private db: AngularFireDatabase)  { }
}
